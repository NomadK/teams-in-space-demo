#!/bin/sh
echo "\n"
echo "Migrating JIRA Data"
DB_PATH=../data/jira/database
H2_JAR=../lib/h2-1.4.185.jar
# shift dates
php migrate_dates.php shiftDatesToPresent $DB_PATH/jiradb-permanent.script $DB_PATH/jiradb.script
# delete h2 database
rm -f $DB_PATH/h2db.mv.db
rm -f $DB_PATH/h2db.trace.db
# create h2 database from jiradb.script
java -cp $H2_JAR org.h2.tools.RunScript -script $DB_PATH/jiradb.script -url "jdbc:h2:file:$DB_PATH/h2db"
# remove jiradb.script
rm $DB_PATH/jiradb.script

echo "\n"
echo "Migrating Confluence Data"
php migrate_dates.php shiftDatesToPresent ../data/confluence/database/confluencedb-permanent.script ../data/confluence/database/confluencedb.script
